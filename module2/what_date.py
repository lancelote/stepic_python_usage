"""
В первой строке дано три числа, соответствующие некоторой дате date -- год,
месяц и день.
Во второй строке дано одно число days -- число дней.

Вычислите и выведите год, месяц и день даты, которая наступит, когда с момента
исходной даты date пройдет число дней, равное days.
"""

import datetime as dt

year, month, day = map(int, input().strip().split(' '))
date = dt.date(year=year, month=month, day=day)

days_since = int(input().strip())

new_date = date + dt.timedelta(days_since)
print('%s %s %s' % (new_date.year, new_date.month, new_date.day))
