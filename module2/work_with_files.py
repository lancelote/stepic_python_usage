"""
Вам дается текстовый файл, содержащий некоторое количество непустых строк.
На основе него сгенерируйте новый текстовый файл, содержащий те же строки в
обратном порядке.
"""

import os

with open(os.path.join('misc', 'dataset.txt')) as data_set:
    lines = data_set.readlines()

with open(os.path.join('misc', 'output.txt'), 'w') as output:
    output.writelines(lines[::-1])
