"""
Вам дана в архиве файловая структура, состоящая из директорий и файлов.

Вам необходимо распаковать этот архив, и затем найти в данной в файловой
структуре все директории, в которых есть хотя бы один файл с расширением ".py".

Ответом на данную задачу будет являться файл со списком таких директорий,
отсортированных в лексикографическом порядке.
"""

import os

dirs_with_py = []

for current_dir, dirs, files in os.walk('main'):
    for file in files:
        if file.endswith('.py'):
            dirs_with_py.append(current_dir)
            break

with open(os.path.join('misc', 'work_with_files_2_output'), 'w') as output:
    output.write('\n'.join(sorted(dirs_with_py)))
