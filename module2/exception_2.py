from collections import defaultdict


def get_family(ancestor, family_dict):
    """Return list of ancestor family (himself, his children and so on"""
    family = [ancestor]
    descendants = family_dict[ancestor]
    if descendants:
        for descendant in descendants:
            family.extend(get_family(descendant, family_dict))
    return family

# Create data structure
exceptions = defaultdict(list)
lines_num = int(input().strip())

for _ in range(lines_num):
    line = input().strip().split(' : ')

    if len(line) != 1:
        child, parents = line[0], line[1].split(' ')
        for parent in parents:
            exceptions[parent].append(child)

# Check useless exceptions
lines_num = int(input().strip())
captured = []

for __ in range(lines_num):
    exception = input().strip()
    if exception not in captured:
        captured.extend(get_family(exception, exceptions))
    else:
        print(exception)
