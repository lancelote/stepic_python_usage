import os
import simplecrypt

with open(os.path.join('misc', 'encrypted.bin'), 'rb') as file:
    cypher_text = file.read()

with open(os.path.join('misc', 'passwords.txt'), 'r') as file:
    passwords = file.read().strip().split('\n')

for password in passwords:
    try:
        text = simplecrypt.decrypt(password, cypher_text)
        print(password, text)
    except simplecrypt.DecryptionException:
        pass
