"""
Одним из самых часто используемых классов в Python является класс filter.
Он принимает в конструкторе два аргумента a и f – последовательность и функцию,
и позволяет проитерироваться только по таким элементам x из последовательности
a, что f(x) равно True. Будем говорить, что в этом случае функция f допускает
элемент x, а элемент x является допущенным.

В данной задаче мы просим вас реализовать класс multifilter, который будет
выполнять ту же функцию, что и стандартный класс filter, но будет использовать
не одну функцию, а несколько.
"""


class multifilter:

    def judge_half(pos, neg):
        """Допускает элемент, если его допускает хотя бы половина фукнций"""
        return pos >= neg

    def judge_any(pos, _):
        """Допускает элемент, если его допускает хотя бы одна функция"""
        return pos >= 1

    def judge_all(_, neg):
        """Допускает элемент, если его допускают все функции"""
        return neg == 0

    def __init__(self, iterable, *funcs, judge=judge_any):
        self.iterable = iterable
        self.funcs = funcs
        self.judge = judge

    def __iter__(self):
        for item in self.iterable:
            judgements = [func(item) for func in self.funcs]
            pos = judgements.count(True)
            neg = judgements.count(False)
            if self.judge(pos, neg):
                yield item
