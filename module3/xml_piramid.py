"""
Вам дано описание пирамиды из кубиков в формате XML.
Кубики могут быть трех цветов: красный (red), зеленый (green) и синий (blue﻿).
Для каждого кубика известны его цвет, и известны кубики, расположенные прямо
под ним.

Пример:

<cube color="blue">
  <cube color="red">
    <cube color="green">
    </cube>
  </cube>
  <cube color="red">
  </cube>
</cube>



Введем понятие ценности для кубиков. Самый верхний кубик, соответствующий
корню XML документа имеет ценность 1. Кубики, расположенные прямо под ним,
имеют ценность 2. Кубики, расположенные прямо под нижележащими кубиками,
имеют ценность 3. И т. д.

Ценность цвета равна сумме ценностей всех кубиков этого цвета.

Выведите через пробел три числа: ценности красного, зеленого и синего цветов.
"""

from collections import defaultdict
from xml.etree import ElementTree

value = defaultdict(int)

data = input().strip()
tree = ElementTree.fromstring(data)


def count_value(element, colors, level):
    for child in element:
        colors[child.attrib['color']] += level
        count_value(child, colors, level + 1)

value[tree.attrib['color']] += 1
count_value(tree, value, 2)
print(value['red'], value['green'], value['blue'])
