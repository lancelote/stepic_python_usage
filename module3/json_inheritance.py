"""
Вам дано описание наследования классов в формате JSON.
Описание представляет из себя массив JSON-объектов, которые соответствуют
классам. У каждого JSON-объекта есть поле name, которое содержит имя класса,
и поле parents, которое содержит список имен прямых предков.
"""

import json
from collections import defaultdict

json_data = input().strip()
data = json.loads(json_data)

nodes = defaultdict(list)

for node in data:
    nodes[node['name']].extend([])
    for parent in node['parents']:
        nodes[parent].append(node['name'])


def get_descendants(item, items):
    descendants = set(items[item])
    for descendant in items[item]:
        descendants = descendants.union(get_descendants(descendant, items))
    return descendants


for node in sorted(nodes.keys()):
    print(node, ':', len(get_descendants(node, nodes)) + 1)
