import pytest
import re

from module3.find_all_domains import get_domain, URL_PATTERN

URL_RECOGNITION = (
    ('html', 'expected'),
    [
        ('<p class = "hello" href= "http://dtepic.org/courses">',
         None),
        ('<link rel="image_src" href="https://examaple.org/files/'
         '6a2/72d/e09/6a272de0944f447fb5972c44cc02f795.png" />',
         None),
        ('<a href="http://stepic.org/courses">',
         'http://stepic.org/courses'),
        ('<a href="ftp://www.mya.ru">',
         'ftp://www.mya.ru'),
        ('<a href=\'https://stepic.org\'>',
         'https://stepic.org'),
        ('<a link href=\'http://neerc.ifmo.ru:1345\'>',
         'http://neerc.ifmo.ru:1345'),
        ('<a target="blank" href=\'http://sasd.ifmo.ru:1345\'>',
         'http://sasd.ifmo.ru:1345'),
        ('<a href=\'http://neerc.ifmo.ru:1345\'>',
         'http://neerc.ifmo.ru:1345'),
        ('<a href="../some_path/index.html">',
         '../some_path/index.html'),
        ('<a href="https://www.ya.ru">',
         'https://www.ya.ru'),
        ('<a href="ftp://mail.ru/distib" >',
         'ftp://mail.ru/distib'),
        ('<a href="bya.ru">',
         'bya.ru'),
        ('<a href="http://www.ya.ru">',
         'http://www.ya.ru'),
        ('<a href="www.kya.ru">',
         'www.kya.ru'),
        ('<a href="../skip_relative_links">',
         '../skip_relative_links'),
        ('<a href="http://stepic.org/courses">',
         'http://stepic.org/courses'),
        ('<a class = "hello" href= "http://ftepic.org/courses" id="dfdf">',
         'http://ftepic.org/courses'),
        ('<a class = "hello" href = "http://a.b.vc.ttepic.org/courses">',
         'http://a.b.vc.ttepic.org/courses'),
        ('<a href=\'https://stepic.org\'>',
         'https://stepic.org'),
        ('<a href=\'http://neerc.ifmo.ru:1345\'>',
         'http://neerc.ifmo.ru:1345'),
        ('<a href=\'http://neerc.ifmo.ru:1345\'>',
         'http://neerc.ifmo.ru:1345'),
        ('<a href = "ftp://mail.ru/distib" >',
         'ftp://mail.ru/distib'),
        ('<a href= "ya.ru">',
         'ya.ru'),
        ('<a href ="www.ya.ru">',
         'www.ya.ru'),
        ('<a href="../skip_relative_links">',
         '../skip_relative_links'),
        ('<a href="http://www.gtu.edu.ge/index_e.htm"'
         'target="_top">Georgian Technical University</a>﻿',
         'http://www.gtu.edu.ge/index_e.htm')
    ]
)

DOMAIN_RECOGNITION = (
    ('url', 'expected'),
    [
        ('http://stepic.org/courses',
         'stepic.org'),
        ('ftp://www.mya.ru',
         'www.mya.ru'),
        ('https://stepic.org',
         'stepic.org'),
        ('http://neerc.ifmo.ru:1345',
         'neerc.ifmo.ru'),
        ('http://sasd.ifmo.ru:1345',
         'sasd.ifmo.ru'),
        ('http://neerc.ifmo.ru:1345',
         'neerc.ifmo.ru'),
        ('../some_path/index.html',
         None),
        ('https://www.ya.ru',
         'www.ya.ru'),
        ('ftp://mail.ru/distib',
         'mail.ru'),
        ('bya.ru',
         'bya.ru'),
        ('http://www.ya.ru',
         'www.ya.ru'),
        ('www.kya.ru',
         'www.kya.ru'),
        ('../skip_relative_links',
         None),
        ('http://stepic.org/courses',
         'stepic.org'),
        ('http://ftepic.org/courses',
         'ftepic.org'),
        ('http://a.b.vc.ttepic.org/courses',
         'a.b.vc.ttepic.org'),
        ('https://stepic.org',
         'stepic.org'),
        ('http://neerc.ifmo.ru:1345',
         'neerc.ifmo.ru'),
        ('http://neerc.ifmo.ru:1345',
         'neerc.ifmo.ru'),
        ('ftp://mail.ru/distib',
         'mail.ru'),
        ('ya.ru',
         'ya.ru'),
        ('www.ya.ru',
         'www.ya.ru'),
        ('../skip_relative_links',
         None),
        ('http://www.gtu.edu.ge/index_e.htm',
         'www.gtu.edu.ge'),
    ]
)


@pytest.mark.parametrize(*URL_RECOGNITION)
def test_find_url(html, expected):
    if expected is None:
        assert re.findall(URL_PATTERN, html) == []
    else:
        assert re.findall(URL_PATTERN, html)[0] == expected


@pytest.mark.parametrize(*DOMAIN_RECOGNITION)
def test_find_domain(url, expected):
    assert get_domain(url) == expected
