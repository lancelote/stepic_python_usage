"""
В этой задаче вам необходимо воспользоваться API сайта numbersapi.com

Вам дается набор чисел. Для каждого из чисел необходимо узнать, существует ли
интересный математический факт об этом числе.

Для каждого числа выведите Interesting, если для числа существует интересный
факт, и Boring иначе.

Выводите информацию об интересности чисел в таком же порядке, в каком следуют
числа во входном файле.
"""

import requests

with open('misc/numbers.txt') as file:
    for line in file:
        num = line.strip()
        url = 'http://numbersapi.com/%s/math?json=true' % num
        response = requests.get(url)
        data = response.json()

        if data['found']:
            print('Interesting')
        else:
            print('Boring')
