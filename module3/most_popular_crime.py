"""
Вам дана частичная выборка из датасета зафиксированных преступлений,
совершенных в городе Чикаго с 2001 года по настоящее время.

Одним из атрибутов преступления является его тип – Primary Type.

Вам необходимо узнать тип преступления, которое было зафиксировано максимальное
число раз в 2015 году.
"""

import csv
from collections import Counter

crimes = []

with open('misc/crimes.csv') as file:
    reader = csv.reader(file)
    for line in reader:
        if '2015' in line[2]:
            crimes.append(line[5])

print(Counter(crimes).most_common(1))
