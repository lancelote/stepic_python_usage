"""
Вашей программе на вход подается ссылка на HTML файл.
Вам необходимо скачать этот файл, затем найти в нем все ссылки вида
<a ... href="..." ... > и вывести список сайтов, на которые есть ссылка.
"""

import re
import requests


URL_PATTERN = r'<a (?:[\w\s="\']+)?href ?=[ \'"\\]+(.+?)[\'"\\]+'


def find_all_urls(url):
    response = requests.get(url)
    urls = re.findall(URL_PATTERN, response.text)
    return urls


def get_domain(url):
    if url.startswith('..'):
        return None
    match = re.search(r'(?:\w+:\/\/)?([\w\.-]+)', url)
    try:
        return match.groups()[0]
    except AttributeError:
        return None


def main():
    start_url = input().strip()
    urls = find_all_urls(start_url)
    return sorted(set([get_domain(url) for url in urls if get_domain(url)]))

# Comment before running tests
for domain in main():
    print(domain)
