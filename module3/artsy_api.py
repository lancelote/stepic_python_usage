"""
Вам даны идентификаторы художников в базе Artsy.

Для каждого идентификатора получите информацию о имени художника и годе
рождения.

Выведите имена художников в порядке неубывания года рождения. В случае если у
художников одинаковый год рождения, выведите их имена в лексикографическом
порядке.
"""

import json
import requests

# Prepare connection
client_id = 'a0c81247e9124e70eb7b'
client_secret = '78c21e9352031ac47e9b22e88c5df70e'

api_url = 'https://api.artsy.net/api/tokens/xapp_token'
params = {
    'client_id': client_id,
    'client_secret': client_secret
}

response = requests.post(api_url, data=params)
data = json.loads(response.text)

token = data['token']

# API usage
artists = []
artists_url = 'https://api.artsy.net/api/artists/%s'
headers = {'X-Xapp-Token': token}

with open('misc/artists.txt') as file:
    for line in file:
        artist = line.strip()
        response = requests.get(artists_url % artist, headers=headers)
        response.encoding = 'utf-8'
        data = json.loads(response.text)
        artists.append((data['sortable_name'], data['birthday']))

for artist in sorted(artists, key=lambda x: int(x[1])):
    print(artist[0])
