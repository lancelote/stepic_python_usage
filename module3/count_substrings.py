"""
Вашей программе на вход подаются две строки s и t, состоящие из строчных
латинских букв.

Выведите одно число – количество вхождений строки t в строку s.
"""

text = input().strip()
substring = input().strip()

count = 0

for i in range(len(text) - len(substring) + 1):
    if text[i:i+len(substring)] == substring:
        count += 1

print(count)
