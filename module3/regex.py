import sys
import re

# Вам дана последовательность строк. Выведите строки, содержащие 'cat' хотя бы
# два раза

for line in sys.stdin:
    if len(re.findall(r'cat', line)) >= 2:
        print(line.strip())

# Вам дана последовательность строк.
# Выведите строки, содержащие "cat" в качестве слова.

for line in sys.stdin:
    if re.search(r'\bcat\b', line):
        print(line.strip())

# Вам дана последовательность строк.
# Выведите строки, содержащие две буквы "z﻿", между которыми ровно три символа.

for line in sys.stdin:
    if re.search(r'z.{3}z', line):
        print(line.strip())

# Вам дана последовательность строк.
# Выведите строки, содержащие обратный слеш "\﻿".

for line in sys.stdin:
    if re.search(r'\\', line):
        print(line.strip())

# Вам дана последовательность строк.
# Выведите строки, содержащие слово, состоящее из двух одинаковых частей
# (тандемный повтор).

for line in sys.stdin:
    if re.search(r'\b(\w+)\1\b', line):
        print(line.strip())

# Вам дана последовательность строк.
# В каждой строке замените все вхождения подстроки "human" на подстроку
# "computer"﻿ и выведите полученные строки.

for line in sys.stdin:
    print(re.sub(r'human', 'computer', line.strip()))

# Вам дана последовательность строк.
# В каждой строке замените первое вхождение слова, состоящего только из
# латинских букв "a" (регистр не важен), на слово "argh".

for line in sys.stdin:
    print(re.sub(r'\b[aA]+\b', 'argh', line.strip(), 1))

# Вам дана последовательность строк.
# В каждой строке поменяйте местами две первых буквы в каждом слове, состоящем
# хотя бы из двух букв.

for line in sys.stdin:
    print(re.sub(r'\b(\w)(\w)', r'\2\1', line.strip()))

# Вам дана последовательность строк.
# В каждой строке замените все вхождения нескольких одинаковых букв на одну
# букву.

for line in sys.stdin:
    print(re.sub(r'(\w)\1+', r'\1', line.strip()))

# Вам дана последовательность строк.
# Выведите строки, содержащие двоичную запись числа, кратного 3.

for line in sys.stdin:
    line = line.strip()
    if re.match(r'^(((0*1)(10*1)*0)(0(10*1)*0|1)*(0(10*1)*(10*))|'
                r'((0*1)(10*1)*(10*)|(0+)))$', line):
        print(line)
