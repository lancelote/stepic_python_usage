"""
Вашей программе на вход подаются две строки, содержащие url двух документов
A и B. Выведите Yes, если из A в B можно перейти за два перехода, иначе
выведите No.
"""

import re
import requests


def find_all_urls(url):
    response = requests.get(url)
    urls = re.findall(r'"(http.+)"', response.text)
    return urls


def main():
    url1 = input().strip()
    url2 = input().strip()

    for url in find_all_urls(url1):
        for sub_url in find_all_urls(url):
            if sub_url == url2:
                return 'Yes'
    return 'No'

print(main())
