"""Count n choose k"""

from math import factorial

n, k = map(int, input().split())
combination_num = factorial(n)/(factorial(k)*factorial(n - k))

print(int(combination_num))
