"""
Реализуйте структуру данных, представляющую собой расширенную структуру стек.
Необходимо поддерживать добавление элемента на вершину стека, удаление с
вершины стека, и необходимо поддерживать операции сложения, вычитания,
умножения и целочисленного деления.

Операция сложения на стеке определяется следующим образом. Со стека снимается
верхний элемент (top1), затем снимается следующий верхний элемент (top2), и
затем как результат операции сложения на вершину стека кладется элемент,
равный top1 + top2.

Аналогичным образом определяются операции вычитания (top1 - top2), умножения
(top1 * top2) и целочисленного деления (top1 // top2).
"""

from operator import add, sub, mul, floordiv


class ExtendedStack(list):

    def apply_to_last(self, operator):
        self.append(operator(self.pop(), self.pop()))

    def sum(self):
        self.apply_to_last(add)

    def sub(self):
        self.apply_to_last(sub)

    def mul(self):
        self.apply_to_last(mul)

    def div(self):
        self.apply_to_last(floordiv)
