"""
Вам дано описание наследования классов в следующем формате.
<имя класса 1> : <имя класса 2> <имя класса 3> ... <имя класса k>
Это означает, что класс 1 отнаследован от класса 2, класса 3, и т. д.

Вам необходимо отвечать на запросы, является ли один класс предком
другого класса

Sample Input:

    4
    A
    B : A
    C : A
    D : B C
    4
    A B
    B D
    C D
    D A

Sample Output:

    Yes
    Yes
    Yes
    No
"""


def is_ancestor(ancestor, descendant):
    if ancestor == descendant:
        return True
    descendant_parents = classes[descendant]
    if not descendant_parents:
        return False
    return next(
        (True for parent in descendant_parents
         if is_ancestor(ancestor, parent)),
        False
    )

classes = dict()

lines_num = int(input().strip())

# Create data structure
for _ in range(lines_num):
    line = input().strip().split(' : ')

    if len(line) == 1:
        classes[line[0]] = dict()
    else:
        child, parents = line[0], line[1].split(' ')
        classes[child] = parents

# Check inheritance
lines_num = int(input().strip())

for __ in range(lines_num):
    class_1, class_2 = input().strip().split(' ')
    print('Yes' if is_ancestor(class_1, class_2) else 'No')
