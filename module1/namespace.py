import sys


class Namespace(object):

    def __init__(self, name, parent=None):
        self.name = name
        self.parent = parent
        self.local_space = []

    def add(self, variable):
        self.local_space.append(variable)

    def get(self, variable):
        if variable in self.local_space:
            return self.name
        elif self.parent:
            return self.parent.get(variable)

namespaces = {'global': Namespace('global')}

for line in sys.stdin:
    if line.strip().isnumeric():
        continue  # Fist useless line with number of input lines

    command, namespace, item = line.strip().split(' ')

    if command == 'create':
        namespaces[namespace] = Namespace(namespace, namespaces[item])
    elif command == 'add':
        namespaces[namespace].add(item)
    elif command == 'get':
        print(namespaces[namespace].get(item))
